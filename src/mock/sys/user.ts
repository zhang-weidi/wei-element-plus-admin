import { MockMethod } from "vite-plugin-mock"
import { Random } from "mockjs"
import { resultError, resultSuccess, resultPageSuccess, RequestParams, baseURL } from "../_util"

const userList = (() => {
  const result: any[] = [
    {
      id: 1,
      account: "admin",
      mobile: "1@integer(2000000000,9999999999)",
      email: "@email",
      nickname: "管理员",
      password: "12345678",
      createTime: "@datetime",
      token: "ff8c9b38-0819-446a-bf8b-ce6ed8c7f140",
      remark: "@cword(10,20)",
      avatar: Random.image("400x400", Random.color(), Random.color(), Random.first()),
      status: "1",
      permissions: [
        {
          menuCode: "dashboard",
          menuName: "仪表盘",
          children: [
            {
              menuCode: "workplace",
              menuName: "工作台",
              children: [],
            },
            {
              menuCode: "monitor",
              menuName: "监控页",
              children: [],
            },
            {
              menuCode: "console",
              menuName: "主控台",
              children: [],
            },
          ],
        },
        {
          menuCode: "system",
          menuName: "系统管理",
          children: [
            {
              menuCode: "user",
              menuName: "用户管理",
              children: [
                {
                  menuCode: "user:add",
                  menuName: "新增权限",
                },
                {
                  menuCode: "user:edit",
                  menuName: "编辑权限",
                },
                {
                  menuCode: "user:delete",
                  menuName: "删除权限",
                },
              ],
            },
            {
              menuCode: "role",
              menuName: "角色管理",
            },
            {
              menuCode: "menu",
              menuName: "菜单管理",
            },
          ],
        },
        {
          menuCode: "iframe",
          menuName: "外部页面",
          children: [
            {
              menuCode: "vuejs",
              menuName: "Vue3文档(内嵌)",
            },
            {
              menuCode: "element-plus",
              menuName: "element plus(内嵌)",
            },
            {
              menuCode: "outside",
              menuName: "跳转外链",
              children: [
                {
                  menuCode: "outside-gitee",
                  menuName: "gitee(外链)",
                },
              ],
            },
          ],
        },
        {
          menuCode: "about",
          menuName: "关于项目",
          children: [
            {
              menuCode: "about-index",
              menuName: "关于项目",
            },
          ],
        },
      ],
    },
  ]
  // 生成mock数据
  for (let i = 0; i < 100; i++) {
    result.push({
      id: `@integer(1000,999999)`,
      account: "@first",
      mobile: "1@integer(2000000000,9999999999)",
      email: "@email",
      nickname: "@cname()",
      password: "@string(6,10)",
      createTime: "@datetime",
      token: "@string(20)",
      remark: "@cword(10,20)",
      "status|1": ["1", "2"],
      avatar: Random.image("400x400", Random.color(), Random.color(), Random.first()),
      permissions: [],
    })
  }
  return result
})()

export default [
  // 用户登录
  {
    url: baseURL + "/sys/user/login",
    timeout: 500,
    method: "post",
    response: (requestParams: RequestParams) => {
      const { account, pwd } = requestParams.body
      const checkUser = userList.find((item) => item.account === account && item.password === pwd)
      if (!checkUser) {
        return resultError("用户密码输入错误")
      }
      return resultSuccess(checkUser)
    },
  },
  // 用户信息
  {
    url: baseURL + "/sys/user/getUser",
    timeout: 200,
    method: "get",
    response: (requestParams: RequestParams) => {
      const token = requestParams.headers?.token
      if (!token) {
        return resultError("没有 token")
      }
      const checkUser = userList.find((item) => item.token === token)
      if (!checkUser) {
        return resultError("没有这个用户")
      }
      return resultSuccess(checkUser)
    },
  },

  {
    url: baseURL + "/sys/user/page",
    timeout: 200,
    method: "get",
    response: (requestParams: RequestParams) => {
      const token = requestParams.headers?.token
      if (!token) {
        return resultError("没有 token")
      }
      const { page = 1, pageSize = 20 } = requestParams.query
      return resultPageSuccess(page, pageSize, userList)
    },
  },
] as MockMethod[]
