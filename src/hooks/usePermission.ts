import { useUserStoreWithOut } from "@/store/modules/user"

export function usePermission() {
  const userStore = useUserStoreWithOut()
  const $auth = (value: string) => {
    return userStore.permissionCodes.includes(value)
  }
  return { $auth }
}
