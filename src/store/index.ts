import type { App } from "vue"
import { createPinia } from "pinia"
import initStoreConfig from "./initStoreConfig"

// 创建 store
const store = createPinia()

export function setupStore(app: App<Element>) {
  // 安装到Vue实例
  app.use(store)
  // 初始化系统内部配置
  initStoreConfig()
}

export { store }
