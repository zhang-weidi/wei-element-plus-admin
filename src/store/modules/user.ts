import { defineStore } from "pinia"
import { store } from "@/store"
import { setStorage, removeStorage, TOKEN_CACHE_KEY, USERINFO_CACHE_KEY } from "@/utils/storage"
import type { RouteRecordRaw } from "vue-router"
import { userLogin, getUserInfo } from "@/api/sys/user"
import {
  UserLoginParams,
  UserLoginResult,
  UserInfoResult,
  PermissionType,
} from "@/api/sys/types/user"
import { useRouteStore } from "./route"

/** 嵌套权限菜单，转为数组Code */
function getPermissionCode(permissions: PermissionType[]) {
  return permissions.reduce((pre: string[], item: PermissionType) => {
    item = Object.assign({}, item)
    pre.push(item.menuCode)
    if (item.children && item.children.length > 0) {
      const arr: string[] = getPermissionCode(item.children)
      if (arr) {
        pre.push(...arr)
      }
    }
    return pre
  }, [])
}
interface UserStateType {
  userInfo: UserInfoResult | undefined
  token: string
  permissionCodes: string[]
}
export const useUserStore = defineStore({
  id: "user",
  state: (): UserStateType => {
    return {
      userInfo: undefined,
      token: "",
      permissionCodes: [],
    }
  },
  getters: {},
  actions: {
    /** 用户登录 */
    login(data: UserLoginParams): Promise<ResponseResult<UserLoginResult>> {
      return new Promise((resolve, reject) => {
        userLogin(data)
          .then((response) => {
            this.token = response.data.token
            setStorage(TOKEN_CACHE_KEY, response.data.token)
            resolve(response)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    /** 用户信息 */
    getUserInfoAction(): Promise<{
      userInfo: UserInfoResult
      permissionCodes: string[]
      asyncRoutes: RouteRecordRaw[]
    }> {
      return getUserInfo()
        .then((res) => {
          const userInfo = res.data
          // 权限码
          const codes = getPermissionCode(res.data.permissions)
          // 路由
          const routeStore = useRouteStore()
          const asyncRoutes = routeStore.setRoutes(codes)
          this.userInfo = userInfo
          this.permissionCodes = codes

          return Promise.resolve({
            userInfo,
            permissionCodes: codes,
            asyncRoutes,
          })
        })
        .catch((err) => {
          return Promise.reject(err)
        })
    },
    /** 登出 */
    logout() {
      return new Promise<void>((resolve, _reject) => {
        // del token
        this.token = ""
        removeStorage(TOKEN_CACHE_KEY)
        // del userInfo
        this.userInfo = undefined
        removeStorage(USERINFO_CACHE_KEY)
        this.permissionCodes = []
        // 清空路由
        const routeStore = useRouteStore()
        routeStore.removeRoutes()
        resolve()
      })
    },
  },
})

/** 需要在设置外部使用 */
export function useUserStoreWithOut() {
  return useUserStore(store)
}
