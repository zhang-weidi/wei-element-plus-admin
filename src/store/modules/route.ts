import { defineStore } from "pinia"
import type { RouteRecordRaw } from "vue-router"
import { store } from "@/store"
import { constantRoutes, notFoundRoute, asyncRoutes } from "@/router/routes"

interface RouteStoreType {
  constantRoutes: RouteRecordRaw[]
  asyncRoutes: RouteRecordRaw[]
  allRoutes: RouteRecordRaw[]
  /** 需要缓存的路由组件名称 */
  keepAliveComponents: string[]
  /** 是否已动态添加路由 */
  isDynamicAddRoute: boolean
}

/** 根据权限code过滤路由菜单 */
function filterAsyncRoutes(routes: RouteRecordRaw[], permissionCodes: string[]) {
  return routes.filter((route) => {
    if (permissionCodes.includes(route.meta?.permissionCode as string)) {
      if (route.children && route.children.length) {
        route.children = filterAsyncRoutes(route.children, permissionCodes)
      }
      return true
    }
    return false
  })
}

export const useRouteStore = defineStore({
  id: "app-route",
  state: (): RouteStoreType => ({
    constantRoutes: [],
    asyncRoutes: [],
    allRoutes: [],
    keepAliveComponents: [],
    isDynamicAddRoute: false,
  }),
  getters: {},
  actions: {
    /**
     * 设置动态路由
     * @param codes 权限标识数组
     * @returns 动态路由
     */
    setRoutes(codes: string[]) {
      const routes = filterAsyncRoutes(asyncRoutes, codes)
      if (routes.length > 0) {
        // 添加根路径重定向记录
        const redirect = routes[0].path
        routes.unshift({
          path: "/",
          redirect: redirect,
          meta: { hideMenu: true },
        })
        // 添加404
        routes.push(notFoundRoute)
        this.asyncRoutes = routes
        this.isDynamicAddRoute = true
      }
      this.constantRoutes = constantRoutes
      this.allRoutes = constantRoutes.concat(routes)
      return routes
    },
    /**
     * 删除路由数据，缓存组件
     */
    removeRoutes() {
      this.constantRoutes = []
      this.asyncRoutes = []
      this.allRoutes = []
      this.keepAliveComponents = []
      this.isDynamicAddRoute = false
    },
  },
})

/** 需要在设置外部使用 */
export function useRouteStoreWithOut() {
  return useRouteStore(store)
}
