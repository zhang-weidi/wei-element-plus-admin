import { defineStore } from "pinia"

export type TabsRouteItem = {
  fullPath: string
  path: string
  name: string
  hash: string
  meta: {
    icon?: string // 菜单图标
    title?: string // 标题
    permissionCode?: string // 权限标记
    hideMenu?: boolean // 当前路由不再菜单显示
    hideChildrenInMenu?: boolean // 当只有一个子路由器时，默认显示子路由器
    frameSrc?: string
  }
  params: object
  query: object
}

export type TabsViewStateType = {
  tabsList: TabsRouteItem[] // 标签页
}

export const useTabsViewStore = defineStore({
  id: "app-tabs-view",
  state: (): TabsViewStateType => ({
    tabsList: [
      // {
      //   fullPath: "/dashboard/workplace",
      //   hash: "",
      //   meta: {
      //     icon: "DashboardOutlined",
      //     title: "工作台",
      //     hideMenu: false,
      //     permissionCode: "workplace",
      //   },
      //   name: "workplace",
      //   params: {},
      //   path: "/dashboard/workplace",
      //   query: {},
      // },
    ],
  }),
  getters: {},
  actions: {
    /**
     * 添加标签页
     * @param route TabsRouteItem
     */
    addTabs(route: TabsRouteItem) {
      this.tabsList.push(route)
    },
    /**
     * 移除标签页
     * @param path 要移除的标签页路径
     */
    removeTabs(path: string) {
      const index = this.tabsList.findIndex((item) => item.fullPath === path)
      this.tabsList.splice(index, 1)
    },
    /**
     * 移除其他标签页
     * @param path 当前路由路径
     */
    removeOtherTabs(path: string) {
      this.tabsList = this.tabsList.filter((item) => item.fullPath === path)
    },
    /**
     * 移除左侧标签页
     * @param path 当前路由路径
     */
    removeLeftTabs(path: string) {
      const index = this.tabsList.findIndex((item) => item.fullPath === path)
      this.tabsList = this.tabsList.filter((item, i) => i >= index)
    },
    /**
     * 移除右侧标签页
     * @param path 当前路由路径
     */
    removeRightTabs(path: string) {
      const index = this.tabsList.findIndex((item) => item.fullPath === path)
      this.tabsList = this.tabsList.filter((item, i) => i <= index)
    },
    /**
     * 移除全部标签页
     */
    removeAllTabs() {
      this.tabsList = []
    },
  },
})
