import { defineStore } from "pinia"
import _ from "lodash"

// 类型字段
interface AppStateType {
  darkTheme: boolean
  // 主题色
  themeColor: string
  // 布局菜单模式 side:右导航 top:顶导航
  mode: string
  // 内容布局模式 fluid:自适应 fixed:固定宽度1200px
  contentWidth: string
  // 导航主题 light:亮色 dark:黑暗
  navTheme: string
  // 顶栏主题 light:亮色 dark:黑暗
  headerTheme: string
  // 固定 Header
  fixedHeader: boolean
  // 固定 Sidebar
  fixedSidebar: boolean
  // 是否禁用移动端模式
  disableMobile: boolean
  // 显示页脚
  showFooter: boolean
  // header高度
  headerHeight: number
  // 侧边菜单收起宽度
  collapsedWidth: number
  // 侧边菜单展开宽度
  siderWidth: number
  // 是否折叠菜单
  collapsed: boolean
  // 路由切换动画
  switchAnimation: string
  // 面包屑
  breadcrumb: boolean
  // 多标签
  multiTabs: boolean
  // 固定多标签页
  // fixedMultiTabs: boolean
  // 项目布局设置弹窗
  settingDrawer: boolean
}

// 项目AppStore配置
export const layoutConfig = {
  darkTheme: false,
  themeColor: "#409EFF",
  mode: "side",
  contentWidth: "fluid",
  navTheme: "dark",
  headerTheme: "light",
  fixedHeader: true,
  fixedSidebar: true,
  disableMobile: false,
  showFooter: false,
  headerHeight: 56,
  collapsedWidth: 64, // 不能修改 element-plus Menu 折叠后宽度就是64
  siderWidth: 216,
  collapsed: false,
  switchAnimation: "zoom",
  breadcrumb: true,
  multiTabs: true,
  // fixedMultiTabs: true,
}

export const useAppStore = defineStore({
  id: "app",
  state: (): AppStateType => {
    return {
      ...layoutConfig,
      settingDrawer: false,
    }
  },
  getters: {
    getLayoutConfig(state) {
      // state = this
      return _.pick(state, _.keys(layoutConfig))
    },
  },
  actions: {},
})
