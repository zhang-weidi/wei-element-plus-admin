import { createRouter, createWebHashHistory } from "vue-router"
import setupRouterGuard from "./router-guards"
import { setupRouterIcon } from "./router-icons"
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { constantRoutes, asyncRoutes, notFoundRoute } from "./routes"
import type { App } from "vue"

// 扩展 RouteMeta 接口来输入 meta 字段
declare module "vue-router" {
  interface RouteMeta {
    icon?: string // 菜单图标
    title?: string // 标题
    permissionCode?: string // 权限标记
    hideMenu?: boolean // 当前路由不再菜单显示
    hideChildrenInMenu?: boolean // 当只有一个子路由器时，默认显示子路由器
    frameSrc?: string
    target?: "_blank" | "_self" // 页面跳转方式 '_blank' | '_self'
    keepAlive?: boolean // 是否缓存路由
    sort?: number // 排序
  }
}

// 创建路由
export const router = createRouter({
  history: createWebHashHistory(),
  routes: constantRoutes,
  // routes: [...constantRoutes, ...asyncRoutes, notFoundRoute],
})

export function setupRouter(app: App<Element>) {
  app.use(router)
  // 设置路由守卫(权限验证)
  setupRouterGuard(router)
  // 注册路由菜单使用到的图标
  setupRouterIcon(app)
}
