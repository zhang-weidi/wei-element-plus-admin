import type { App } from "vue"
import { DashboardOutlined, LinkOutlined, ProjectOutlined } from "@vicons/antd"
import { Settings20Regular } from "@vicons/fluent"

export const routerIconMap = {
  DashboardOutlined,
  Settings20Regular,
  LinkOutlined,
  ProjectOutlined,
}

export function setupRouterIcon(app: App<Element>) {
  // for (const [key, component] of Object.entries(routerIconMap)) {
  //   app.component(key, component)
  // }
  Object.entries(routerIconMap).forEach(([key, component]) => {
    app.component(key, component)
  })
}
