import type { Router } from "vue-router"
import { useRouteStore } from "@/store/modules/route"
import { useUserStore } from "@/store/modules/user"
import { REDIRECT_ROUTE_NAME, LOGIN_ROUTE_PATH, routeAllowList } from "@/router/routes"
import NProgress from "nprogress"
import "@/styles/nprogress.css"
import { ElNotification } from "element-plus"

export default function setupRouterGuard(router: Router) {
  NProgress.configure({
    showSpinner: false,
    // speed: 5000,
  })
  const routeStore = useRouteStore()
  const userStore = useUserStore()
  // 全局前置守卫
  router.beforeEach(async (to, from, next) => {
    NProgress.start()
    // 有token并且要进入的页面是登录页
    if (userStore.token && to.path === LOGIN_ROUTE_PATH) {
      // 判断是否带有重定向
      next({ path: decodeURIComponent((to.query.redirect || "/dashboard") as string) })
      NProgress.done()
      return
    }

    if (routeAllowList.includes(to.name as string)) {
      // 在免登录名单，直接进入
      return next()
    }

    if (!userStore.token) {
      // 没有登录，跳转登录页面
      next({ path: LOGIN_ROUTE_PATH, query: { redirect: encodeURIComponent(to.fullPath) } })
      NProgress.done()
      return
    }

    if (routeStore.isDynamicAddRoute) {
      // 已添加动态路由
      return next()
    }

    // 获取用户信息，添加动态路由
    try {
      const res = await userStore.getUserInfoAction()
      res.asyncRoutes.forEach((route: any) => {
        // 动态添加可访问路由表
        router.addRoute(route)
      })
      // 动态添加路由后，此处应当重定向到fullPath，否则会加载404页面内容
      next({ ...to, replace: true })
      return
    } catch (error) {
      ElNotification({
        title: "错误",
        message: `请求用户信息失败，请重试`,
        type: "error",
      })
      try {
        await userStore.logout()
        next({
          path: LOGIN_ROUTE_PATH,
          query: { redirect: encodeURIComponent(to.fullPath) },
        })
        NProgress.done()
        return
      } catch (error) {
        next("/error/500")
      }
    }
  })

  // 全局后置钩子
  router.afterEach((to) => {
    // 设置标题
    document.title = (to?.meta?.title as string) || document.title

    // 设置需要缓存的组件名称
    const cacheCom = routeStore.keepAliveComponents
    if (to.name && to.meta.keepAlive && !cacheCom.includes(to.name as string)) {
      // 需要缓存的组件
      cacheCom.push(to.name as string)
    } else if (!to.meta.keepAlive || to.name === REDIRECT_ROUTE_NAME) {
      // 不需要缓存的组件
      const index = cacheCom.findIndex((name) => name === to.name)
      if (index !== -1) {
        cacheCom.splice(index, 1)
      }
    }

    NProgress.done()
  })
}
