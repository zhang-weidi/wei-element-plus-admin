import type { RouteRecordRaw } from "vue-router"
import Layout from "@/layouts/layout/Layout.vue"
// import BlankLayout from '@/layouts/BlankLayout.vue'; // 空白页承载嵌套路由

const routes: Array<RouteRecordRaw> = [
  {
    path: "/system",
    name: "System",
    meta: {
      icon: "Settings20Regular",
      title: "系统设置",
      hideMenu: false,
      permissionCode: "system",
      sort: 20,
    },
    component: Layout,
    redirect: "/system/user",
    children: [
      {
        path: "user",
        name: "User",
        meta: {
          title: "用户管理",
          hideMenu: false,
          permissionCode: "user",
        },
        component: () => import("@/views/system/user/User.vue"),
      },
      {
        path: "role",
        name: "Role",
        meta: {
          title: "角色管理",
          hideMenu: false,
          permissionCode: "role",
        },
        component: () => import("@/views/system/role/Role.vue"),
      },
      {
        path: "menu",
        name: "Menu",
        meta: {
          title: "菜单管理",
          hideMenu: false,
          permissionCode: "menu",
        },
        component: () => import("@/views/system/menu/Menu.vue"),
      },
    ],
  },
]

export default routes
