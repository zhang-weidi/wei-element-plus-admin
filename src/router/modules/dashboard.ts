import type { RouteRecordRaw } from "vue-router"
import Layout from "@/layouts/layout/Layout.vue"

const routes: Array<RouteRecordRaw> = [
  {
    path: "/dashboard",
    name: "Dashboard",
    meta: {
      // icon: markRaw(DashboardOutlined),
      icon: "DashboardOutlined",
      title: "仪表盘",
      hideMenu: false,
      permissionCode: "dashboard",
      sort: 10,
    },
    component: Layout,
    redirect: "/dashboard/workplace",
    children: [
      {
        path: "workplace",
        name: "Workplace",
        meta: {
          title: "工作台",
          hideMenu: false,
          permissionCode: "workplace",
          keepAlive: true,
          sort: 2,
        },
        component: () => import("@/views/dashboard/workplace/Workplace.vue"),
      },
      {
        path: "monitor",
        name: "Monitor",
        meta: {
          title: "监控页",
          hideMenu: false,
          permissionCode: "monitor",
          keepAlive: true,
          sort: 3,
        },
        component: () => import("@/views/dashboard/monitor/Monitor.vue"),
      },
      {
        path: "console",
        name: "Console",
        meta: {
          title: "主控台",
          hideMenu: false,
          permissionCode: "console",
          keepAlive: true,
          sort: 6,
        },
        component: () => import("@/views/dashboard/console/Console.vue"),
      },
    ],
  },
]
export default routes
