import type { RouteRecordRaw } from "vue-router"
import Layout from "@/layouts/layout/Layout.vue"
// import BlankLayout from '@/layouts/BlankLayout.vue'; // 空白页承载嵌套路由

const routes: Array<RouteRecordRaw> = [
  {
    path: "/about",
    name: "About",
    meta: {
      hideChildrenInMenu: true,
      permissionCode: "about",
      sort: 22,
    },
    component: Layout,
    redirect: "/system/user",
    children: [
      {
        path: "index",
        name: "AboutIndex",
        meta: {
          title: "关于项目",
          permissionCode: "about-index",
          icon: "ProjectOutlined",
        },
        component: () => import("@/views/about/About.vue"),
      },
    ],
  },
]

export default routes
