import type { RouteRecordRaw } from "vue-router"
import Layout from "@/layouts/layout/Layout.vue"
import BlankLayout from "@/layouts/BlankLayout.vue" // 空白页承载嵌套路由

const IFrame = () => import("@/views/iframe/IFrameView.vue")

const routes: RouteRecordRaw = {
  path: "/iframe",
  name: "IFrame",
  meta: {
    icon: "LinkOutlined",
    title: "外部页面",
    hideMenu: false,
    permissionCode: "iframe",
    sort: 21,
  },
  component: Layout,
  redirect: "/iframe/vuejs",
  children: [
    {
      path: "vuejs",
      name: "Vuejs",
      meta: {
        title: "Vue3文档(内嵌)",
        hideMenu: false,
        permissionCode: "vuejs",
        frameSrc: "https://cn.vuejs.org/",
      },
      component: IFrame,
    },
    {
      path: "element-plus",
      name: "ElementPlus",
      meta: {
        title: "element plus(内嵌)",
        hideMenu: false,
        permissionCode: "element-plus",
        frameSrc: "https://element-plus.org",
      },
      component: IFrame,
    },
    {
      path: "outside",
      name: "Outside",
      meta: {
        title: "跳转外链",
        hideMenu: false,
        permissionCode: "outside",
      },
      component: BlankLayout,
      children: [
        {
          path: "https://gitee.com/zhang-weidi",
          name: "outside-gitee",
          component: BlankLayout,
          meta: {
            title: "gitee(外链)",
            target: "_blank",
            permissionCode: "outside-gitee",
          },
        },
      ],
    },
  ],
}

// 跳转外链
// {
//   path: '/web-url',
//   name: 'web-url',
//   meta: {
//     hideChildrenInMenu: true,
//     permissionCode: 'user',
//   },
//   component: Layout,
//   redirect: '/web-url',
//   children: [
//     {
//       path: 'https://v3.cn.vuejs.org',
//       name: 'web-vue',
//       component: BlankLayout,
//       meta: {
//         icon: 'icon-jiankongfuwu',
//         title: '外链 Vue',
//         target: '_blank',
//         permissionCode: 'user',
//       },
//     },
//   ],
// },

export default routes
