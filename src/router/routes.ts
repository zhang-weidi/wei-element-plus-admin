import Layout from "@/layouts/layout/Layout.vue"
// import BlankLayout from '@/layouts/BlankLayout.vue'; // 空白页承载嵌套路由
import ErrorPage from "@/views/exception/ErrorPage.vue"
import type { RouteRecordRaw } from "vue-router"

/** 重定向路由名称(刷新页面) */
export const REDIRECT_ROUTE_NAME = "RedirectView"

/** 登录页面路径 */
export const LOGIN_ROUTE_PATH = "/user/login"

/** 白名单页面(不验证权限) */
export const routeAllowList = ["Login"]

/** 不需要出现在标签页中的路由 */
export const noAddTabsList = ["Redirect", REDIRECT_ROUTE_NAME, "Login"]

/** 路由排序 */
export function routeSort(routes: RouteRecordRaw[]) {
  return routes
    .map((route) => {
      if (route.children && route.children.length > 0) {
        route.children = routeSort(route.children)
      }
      return route
    })
    .sort((a: RouteRecordRaw, b: RouteRecordRaw) => (a.meta?.sort || 0) - (b.meta?.sort || 0))
}

/** 404 */
export const notFoundRoute: RouteRecordRaw = {
  path: "/:path(.*)*",
  name: "NotFound",
  component: ErrorPage,
  meta: {
    hideMenu: true,
  },
}

/**
 * 基础路由(不验证权限)
 * @param name 路由名称, 必须设置,且不能重名
 * @param meta 路由元信息（路由扩展信息）
 * @param redirect 重定向地址
 * @param meta.icon 菜单图标
 * @param meta.title 菜单名称
 * @param meta.permissionCode 权限标记
 * @param meta.hideMenu 当前路由不再菜单显示
 * @param meta.hideChildrenInMenu 当只有一个子路由器时，默认显示子路由器
 * @param meta.frameSrc 内嵌页面地址
 * @param meta.target 跳转方式
 * @param meta.keepAlive 缓存该路由
 * @param meta.sort 排序越小越排前(升序)
 *
 * */
export const constantRoutes: RouteRecordRaw[] = [
  /* {
    path: '', // 路由
    name: '', // 路由name(命名规则同组件名称使用大驼峰)，如果路由组件需要缓存，需要同组件name一样
    meta: {
      icon: '', // 菜单图标
      title: '', // 标题
      hideMenu: false, // 当前路由不再菜单显示
      hideChildrenInMenu: false, // 当只有一个子路由器时，默认显示子路由器
      frameSrc: "" // 嵌套页面跳转地址
    },
    component: Layout,
    redirect: '/workbench/index',
  }, */

  // {
  //   path: "/",
  //   redirect: "/dashboard",
  //   meta: {
  //     hideMenu: true,
  //   },
  // },

  // 登录页
  {
    path: LOGIN_ROUTE_PATH,
    name: "Login",
    component: () => import("@/views/login/Login.vue"),
    meta: {
      hideMenu: true,
    },
  },

  // 错误页面
  {
    path: "/error/500",
    name: "Error500",
    component: ErrorPage,
    props: {
      status: 500,
    },
    meta: {
      hideMenu: true,
    },
  },
  {
    path: "/error/404",
    name: "Error404",
    component: ErrorPage,
    meta: {
      hideMenu: true,
    },
  },
  // 重定向路由(刷新页面)
  {
    path: "/redirect",
    name: "Redirect",
    component: Layout,
    meta: {
      title: "重定向",
      hideMenu: true,
    },
    children: [
      {
        path: "/redirect/:path(.*)",
        name: REDIRECT_ROUTE_NAME,
        component: () => import("@/views/redirect/index.vue"),
        meta: {
          hideMenu: true,
        },
      },
    ],
  },
]

// 动态路由(需要验证权限)
// 读取modules文件夹下导出的路由模块
const modulesFiles = import.meta.glob("./modules/**/*.ts", { eager: true })
const modules: any[] = []
Object.values(modulesFiles).forEach((value) => {
  const mod = (value as any).default || {}
  const modList = Array.isArray(mod) ? [...mod] : [mod]
  modules.push(...modList)
})
/* const modulesFiles = import.meta.glob("./modules/*.js", { eager: true }) // 异步方式
let modules = {}
for (const [key, value] of Object.entries(modulesFiles)) {
  //名称  因为这里拿到的是  ./modules/app.js ，所以需要两层处理
  const moduleName = key.replace(/^\.\/(.*)\.\w+$/, "$1")
  const name = moduleName.split("/")[1]
  //具体的内容，都是每个js中返回值  value.default
  modules[name] = value.default
} */
export const asyncRoutes: RouteRecordRaw[] = routeSort(modules)
