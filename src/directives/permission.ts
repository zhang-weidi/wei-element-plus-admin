import { usePermission } from "@/hooks/usePermission"
import { ObjectDirective } from "vue"

const permission: ObjectDirective = {
  // 绑定元素的父组件被挂载后调用
  mounted(el, binding: any) {
    const { $auth } = usePermission()
    if (!$auth(binding.value)) {
      // 获取el父节点在removeChild移除子节点el
      el.parentNode?.removeChild(el)
    }
  },
}

export default permission
