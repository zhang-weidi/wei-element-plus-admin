import type { App } from "vue"
import permission from "./permission"

/**
 * 配置和注册全局指令
 */
export function setupGlobalDirectives(app: App) {
  // 权限控制指令
  app.directive("auth", permission)
}
