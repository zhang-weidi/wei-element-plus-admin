/** 登录请求参数 */
export interface UserLoginParams {
  account: string
  pwd: string
}

/**
 * 登录响应结果
 */
export interface UserLoginResult {
  name: string
  token: string
}

/**
 * 用户信息
 */
export interface UserInfoResult {
  id: number
  account: string // 账号
  mobile: string
  email: string
  nickname: string // 姓名
  password: string
  createTime: string // 创建时间
  token: string
  status: number // 状态
  permissions: PermissionType[]
}

/**
 * 权限
 */
export interface PermissionType {
  menuCode: string
  /** 菜单名称 */
  menuName: string
  /** 菜单类型：1:菜单；2：按钮 */
  type: string
  children: PermissionType[]
}

/**
 * 用户列表 请求参数
 */
export interface UserPageListParams {
  page: number // 当前页
  pageSize: number // 分页数
  nickname: string // 用户名
  mobile: string // 账号
  status: string // 状态
}

/**
 * 用户表格type
 */
export interface UserTableDataType {
  id: number
  account: string
  mobile: string
  email: string
  nickname: string
  password: string
  createTime: string
  token: string
  remark: string
  status: string
}
