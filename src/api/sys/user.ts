import request from "@/utils/request"
import {
  UserLoginParams,
  UserLoginResult,
  UserInfoResult,
  UserPageListParams,
  UserTableDataType,
} from "./types/user"

/** 登录 */
export const userLogin = (data: UserLoginParams) => {
  // 通过泛型，定义request返回结果
  return request<UserLoginResult>({
    url: "/sys/user/login",
    method: "post",
    data,
    hideErrorTips: true,
  })
}

/**
 * 获取用户信息
 */
export const getUserInfo = () => {
  return request<UserInfoResult>({
    url: "/sys/user/getUser",
    method: "get",
    hideErrorTips: true,
    // params: {}
  })
}

/** 刷新token */
export const refreshToken = (data: any) => {
  return request<{ refToken: string }>({
    url: "/sys/user/ref/token",
    method: "post",
    data,
    hideErrorTips: true,
  })
}

/** 用户列表 */
export const getUserPageList = (data: UserPageListParams) => {
  return request<PageListResult<UserTableDataType>>({
    url: "/sys/user/page",
    method: "get",
    params: data,
  })
}
