import { createApp } from "vue"
// 引入 tailwind 需要在App.vue文件之前引入，防止覆盖element-plus组件样式
import "@/styles/tailwind.css"

import App from "./App.vue"
import { setupStore } from "@/store"
import { setupRouter } from "@/router"
import { setupGlobalComponents } from "@/components"
import { setupGlobalDirectives } from "@/directives"

// 重置 自定义样式
import "@/styles/index.less"

function bootstrap() {
  const app = createApp(App)
  // 安装全局状态管理 pinia
  setupStore(app)
  // 安装路由
  setupRouter(app)
  // 安装全局组件
  setupGlobalComponents(app)
  // 安装全局指令
  setupGlobalDirectives(app)
  // 挂载应用
  app.mount("#app")
}

bootstrap()
