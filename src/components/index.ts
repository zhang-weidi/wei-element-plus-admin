import type { App } from "vue"
import FullScreenDialog from "./FullScreenDialog.vue"

/** TypeScript 全局组件类型申明 */
declare module "@vue/runtime-core" {
  export interface GlobalComponents {
    FullScreenDialog: typeof FullScreenDialog
  }
}

/**
 * 配置和注册全局组件
 */
export function setupGlobalComponents(app: App) {
  app.component("FullScreenDialog", FullScreenDialog)
}
