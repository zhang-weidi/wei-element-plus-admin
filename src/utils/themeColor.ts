// https://juejin.cn/post/7024025899813044232
const html = document.documentElement

/** 变量前缀 */
const PRE = "--el-color-primary"
const PRE_LIGHT = `${PRE}-light`
const PRE_DARK = `${PRE}-dark`

/** 根据主色调生成次色调 */
export function setColorMixLevel(color1: string, color2: string, weight: number) {
  weight = Math.max(Math.min(Number(weight), 1), 0)
  const r1 = parseInt(color1.substring(1, 3), 16)
  const g1 = parseInt(color1.substring(3, 5), 16)
  const b1 = parseInt(color1.substring(5, 7), 16)
  const r2 = parseInt(color2.substring(1, 3), 16)
  const g2 = parseInt(color2.substring(3, 5), 16)
  const b2 = parseInt(color2.substring(5, 7), 16)
  const r = Math.round(r1 * (1 - weight) + r2 * weight)
  const g = Math.round(g1 * (1 - weight) + g2 * weight)
  const b = Math.round(b1 * (1 - weight) + b2 * weight)
  const _r = ("0" + (r || 0).toString(16)).slice(-2)
  const _g = ("0" + (g || 0).toString(16)).slice(-2)
  const _b = ("0" + (b || 0).toString(16)).slice(-2)
  return "#" + _r + _g + _b
}

/** 设置亮色主题使用的色调 */
export function setLightThemeColor(color?: string) {
  if (!color) {
    return
  }
  /** 白色 */
  const WHITE = "#ffffff"
  /** 黑色 */
  const BLACK = "#000000"

  // 设置主要颜色
  html.style.setProperty(PRE, color)
  // // 循环设置次级颜色
  for (let i = 1; i < 10; i += 1) {
    html.style.setProperty(`${PRE_LIGHT}-${i}`, setColorMixLevel(color, WHITE, i * 0.1))
  }
  // 设置主要暗色
  html.style.setProperty(`${PRE_DARK}-2`, setColorMixLevel(color, BLACK, 0.2))
}

/** 设置暗色主题使用的色调 */
export function setDarkThemeColor(color?: string) {
  if (!color) {
    return
  }
  /** 黑色 */
  const BLACK = "#141414"
  /** 白色 */
  const WHITE = "#ffffff"

  // 设置主要颜色
  html.style.setProperty(PRE, color)
  // // 循环设置次级颜色
  for (let i = 1; i < 10; i += 1) {
    html.style.setProperty(`${PRE_LIGHT}-${i}`, setColorMixLevel(color, BLACK, i * 0.1))
  }
  // 设置主要暗色
  html.style.setProperty(`${PRE_DARK}-2`, setColorMixLevel(color, WHITE, 0.2))
}
