/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        // css3变量实现动态主题
        primary: "var(--el-color-primary)",
        success: "var(--el-color-success)",
        warning: "var(--el-color-warning)",
        danger: "var(--el-color-danger)",
        error: "var(--el-color-error)",
        info: "var(--el-color-info)",
      },
    },
    // 设置 tailwindCSS 响应断点同 element-plus 一样
    screens: {
      // xs: "",
      sm: "768px",
      md: "992px",
      lg: "1200px",
      xl: "1920px",
    },
  },
  plugins: [],
}
