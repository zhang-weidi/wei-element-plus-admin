// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { defineConfig, loadEnv } from "vite"
import vue from "@vitejs/plugin-vue"
import vueJsx from "@vitejs/plugin-vue-jsx"
import path from "path"
import DefineOptions from "unplugin-vue-define-options/vite"

import AutoImport from "unplugin-auto-import/vite"
import Components from "unplugin-vue-components/vite"
import ElementPlus from "unplugin-element-plus/vite"
import { ElementPlusResolver } from "unplugin-vue-components/resolvers"

// mock 模拟数据
import { viteMockServe } from "vite-plugin-mock"

// https://vitejs.dev/config/
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default defineConfig(({ command, mode }) => {
  // const env = loadEnv(mode, process.cwd())

  return {
    base: "./",
    plugins: [
      vue(),
      // Vue3 JSX支持，目前没有用上，可能后面会用到，先装上吧
      vueJsx(),
      DefineOptions(),
      // 自动导入不需要再template模板中使用的组件的样式 例如：ElMessage
      ElementPlus({
        // options
      }),
      AutoImport({
        resolvers: [ElementPlusResolver()],
      }),
      Components({
        resolvers: [ElementPlusResolver()],
      }),

      viteMockServe({
        ignore: /^_/,
        mockPath: "./src/mock",
        localEnabled: command === "serve",
        prodEnabled: command !== "serve",
        injectCode: `
          import { setupProdMockServer } from '@/mock/_createProductionServer';

          setupProdMockServer();
        `,
      }),
    ],
    resolve: {
      alias: {
        // 别名配置
        "@": path.resolve(__dirname, "src"),
      },
    },
  }
})
