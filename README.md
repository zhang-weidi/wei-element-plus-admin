# 简介
wei-element-plus-admin 一个基于vite4，vue3.2+，pinia，vue-router，element-plus，tailwindcss，axios后台管理系统框架，目前只完成的基础模板搭建，后面会慢慢补充功能点

# 使用
- 安装依赖
```
yarn install
```
- 运行
```
yarn dev
```
- 打包
```
yarn build
```
- 预览
```
yarn preview
```
- 打包测试环境
```
yarn build:staging
```

# 开发中遇到的坑

## `<script setup>`语法糖，设置 name 实现路由缓存的问题 
- 缓存组件`<KeepAlive>`通过其 `include` / `exclude` prop 来匹配其需要缓存的组件时，组件的名称必须声明

- vue3.2.34或以上的版本中，使用 `<script setup>` 的单文件组件会自动根据文件名生成对应的 name 选项，无需再手动声明，如果需要手动声明有如下两种方式

方式1：单独写一个 `script block` 用于设置name
``` ts
<script lang="ts">
  export default {
    name: "CustomName",
  }
</script>

<script setup lang="ts">
  // script setup logic
</script>
```

方式2：使用unplugin-vue-define-options插件
> 安装插件
``` 
yarn add unplugin-vue-define-options --dev
```
> 在vite.config.ts中配置
```ts
// vite.config.ts
import DefineOptions from 'unplugin-vue-define-options/vite'

export default defineConfig({
  plugins: [DefineOptions()],
})
```
> 如果是ts项目需要在tsconfig.json中添加一个配置
``` ts
{
  "compilerOptions": {
    "types": ["unplugin-vue-define-options"]
  }
}
```
> vue中使用
``` ts
<script setup lang="ts">
  defineOptions({
    name: 'App'
  })
</script>
```

## TS中通过变量存储key值读取对象的属性值时报错(TS： 7053)
可以使用 ts 的 keyof 和 typeof 混合使用
``` ts
const obj = {
  name: "张炜迪",
  age: 25,
}
Object.keys(obj).forEach((key) => {
  console.log(`key=${key}, value=${obj[key as keyof typeof obj]}`)
})
```
## [Mock模拟数据](https://github.com/vbenjs/vite-plugin-mock/blob/main/README.zh_CN.md)

安装插件
```js
yarn add mockjs

yarn add vite-plugin-mock -D
```
创建mock文件夹，并在mock文件夹下创建_createProductionServer.ts，在文件中添加如下代码
```ts
import { createProdMockServer } from "vite-plugin-mock/es/createProdMockServer"

const modules = import.meta.glob("./modules/**/*.ts", { eager: true })

const mockModules: any[] = []
Object.keys(modules).forEach((key) => {
  if (key.includes("/_")) {
    return
  }
  mockModules.push(...(modules[key] as any).default)
})

/**
 * 用于生产环境。需要手动导入所有模块
 */
export function setupProdMockServer() {
  createProdMockServer(mockModules)
}
```
vite.config.ts 配置
```ts
import { defineConfig } from "vite"

// mock 模拟数据
import { viteMockServe } from "vite-plugin-mock"

export default defineConfig({
  plugins: [
    viteMockServe({
      ignore: /^_/,
      mockPath: "./src/mock",
      injectCode: `
        import { setupProdMockServer } from '@/mock/_createProductionServer';

        setupProdMockServer();
      `,
    }),
  ],
})
```
添加请求模拟接口：在mock文件夹下创建user.ts文件
```ts
export default [
  // 用户信息
  {
    url: "/sys/user/getUser",
    timeout: 200,
    method: "get",
    response: (requestParams: any) => {
      // 请求返回的数据
      return {
        code: 200,
        data: {},
        message: "success",
      }
    },
  },
] as MockMethod[]
```

## .d.ts导入import其它类型导致全局类型失效问题
原因在于，在一个.d.ts文件中引入了其他模块，就会使ts类型系统将其视为一个模块，导致其中的所有类型即使添加了declare关键字也不能直接全局使用
```ts
// 解决方式使用 declare global {}
declare global {
  // 全局类型代码
} 
```

# 文件说明

